const { UserModel, BodyPartModel } = require("../db/dbaccessor");
const jwt = require("jsonwebtoken");
const dotenv = require("dotenv");
dotenv.config();

const login = async (user, res) => {
  let result = await UserModel.findOne(user);
  if (result) {
    result.password = "";
    res.json({
      code: 200,
      data: {
        content: {
          token: generateToken(result),
          user: result,
        },
      },
    });
  } else {
    res.status(404).json({
      code: 500,
    });
  }
};

const register = async (req, res, user) => {
  user = await loadBodyPart(user);
  console.log(user);
  let userModel = new UserModel(user);
  userModel
    .save()
    .then(() => {
      user = {
        email: user.email,
        password: user.password,
      };
      login(user, res);
    })
    .catch((err) => {
      res.send({
        code: 500,
        text: `Error durant l'inscription`,
      });
    });
};

const registerClient = async (req, res) => {
  let user = req.body;
  user.accreditation = process.env.CLIENT_ACCREDITATION;
  await register(req, res, user);
};

const registerEmployee = async (req, res) => {
  let user = req.body;
  user.accreditation = process.env.EMPLOYEE_ACCREDITATION;
  await register(req, res, user);
};

const registerAdmin = async (req, res) => {
  let user = req.body;
  user.accreditation = process.env.ADMIN_ACCREDITATION;
  await register(req, res, user);
};

const loadBodyPart = async (user) => {
  user.bodyPart = await Promise.all(
    user.bodyPart.map(async (part) => {
      let bodyPart = await BodyPartModel.findById(part._id);
      bodyPart.types = await Promise.all(
        bodyPart.types.filter((t) => {
          for (let i = 0; i < part.types.length; i++) {
            if (part.types[i]._id === t._id.toString()) {
              return true;
            }
          }
          return false;
        })
      );
      return bodyPart;
    })
  );
  return user;
};

const chechToken = (req) => {
  let tokenHeaderKey = process.env.TOKEN_HEADER_KEY;
  let jwtSecretKey = process.env.JWT_SECRET_KEY;
  try {
    const token = req.header(tokenHeaderKey);
    return jwt.verify(token, jwtSecretKey);
  } catch (error) {
    return false;
  }
};

const checkAccreditation = async (req, accreditation) => {
  if (!chechToken(req)) {
    return false;
  }
  let userId = process.env.USER_ID;
  let user = {
    _id: req.header(userId),
  };
  user = await UserModel.findOne(user);
  console.log(user);
  console.log(accreditation);
  console.log(user.accreditation >= accreditation);
  return user.accreditation >= accreditation;
};

const generateToken = (user) => {
  let jwtSecretKey = process.env.JWT_SECRET_KEY;
  let expiryValue = parseFloat(process.env.TOKEN_EXPIRY_HOURS, 10);
  return jwt.sign(
    {
      time: new Date(),
      password_id: user.password,
      exp: Math.floor(Date.now() / 1000) + 3600 * expiryValue,
    },
    jwtSecretKey
  );
};

const getBodyParts = async (req, res) => {
  let result = await BodyPartModel.find();
  res.send({
    code: 200,
    data: result,
  });
};

exports.login = login;
exports.bodyParts = getBodyParts;
exports.registerClient = registerClient;
exports.registerEmployee = registerEmployee;
exports.registerAdmin = registerAdmin;
exports.chechToken = chechToken;
exports.checkAccreditation = checkAccreditation;
