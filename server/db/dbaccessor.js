const mongoose = require("mongoose");
const { ObjectId, Decimal128 } = require("mongodb");
const { time } = require("console");
const timeSlot = new mongoose.Schema(
  {
    start: String,
    end: String,
  },
  {
    collection: "timeSlot",
  }
);
const commission = new mongoose.Schema(
  {
    serviceId: ObjectId,
    value: Decimal128,
  },
  {
    collection: "commission",
  }
);

const partType = new mongoose.Schema({
  title: String,
});
const bodyPart = new mongoose.Schema(
  {
    title: String,
    types: [partType],
  },
  {
    collection: "bodypart",
  }
);
const skill = new mongoose.Schema(
  {
    title: String,
  },
  {
    collection: "skill",
  }
);
const user = new mongoose.Schema(
  {
    name: String,
    firstname: String,
    password: String,
    email: String,
    accreditation: Number,
    birthdate: Date,
    photo: String,
    salary: Number,
    hourly: [timeSlot],
    commission: [commission],
    skills: [skill],
    bodyPart: [bodyPart],
  },
  {
    collection: "auth",
  }
);

const pricepertype = new mongoose.Schema({
  type: partType,
  price: Number,
});
const price = new mongoose.Schema({
  bodyPart: bodyPart,
  pricesPerType: [pricepertype],
});
const service = new mongoose.Schema({
  title: String,
  duration: Date,
  time: String,
  minprice: Number,
  maxprice: Number,
  skills: [skill],
  prices: [price],
  commission: Number,
});

const appointment = new mongoose.Schema(
  {
    service: service,
    user: user,
    employee: user,
    date: Date,
    state: Number,
    price: Number,
  },
  {
    collection: "appointment",
  }
);

const spentType = new mongoose.Schema(
  {
    title: String,
  },
  {
    collection: "spentType",
  }
);

const spent = new mongoose.Schema(
  {
    date: Date,
    type: spentType,
    value: Number,
    description: String,
  },
  {
    collection: "spent",
  }
);

const transactions = new mongoose.Schema(
  {
    user: user,
    value: Number,
    state: Number,
    reste: Number,
    date : Date
  },
  {
    collection: "spent",
  }
);

const askingSold = new mongoose.Schema(
  {
    user: user,
    value: Number,
    date : Date
  },
  {
    collection: "askingSold",
  }
);

UserModel = mongoose.model("User", user);
SkillModel = mongoose.model("Skill", skill);
BodyPartModel = mongoose.model("BodyPart", bodyPart);
TimeSlotModel = mongoose.model("TimeSlot", timeSlot);
PriceModel = mongoose.model("Price", price);
CommissionModel = mongoose.model("Commission", commission);
PartTypeModel = mongoose.model("PartType", partType);
PriceTypeModel = mongoose.model("PricePerType", pricepertype);
ServiceModel = mongoose.model("Service", service);
AppointmentModel = mongoose.model("Appointment", appointment);
SpentTypeModel = mongoose.model("SpentType", spentType);
SpentModel = mongoose.model("Spent", spent);
TransactionModel = mongoose.model("Transaction", transactions);
AskingSoldModel = mongoose.model("AskingSold", askingSold);
module.exports.ServiceModel = ServiceModel;
module.exports.SkillModel = SkillModel;
module.exports.UserModel = UserModel;
module.exports.BodyPartModel = BodyPartModel;
module.exports.TimeSlotModel = TimeSlotModel;
module.exports.PriceModel = PriceModel;
module.exports.CommissionModel = CommissionModel;
module.exports.PartTypeModel = PartTypeModel;
module.exports.PriceTypeModel = PriceTypeModel;
module.exports.ServiceModel = ServiceModel;
module.exports.AppointmentModel = AppointmentModel;
module.exports.SpentTypeModel = SpentTypeModel;
module.exports.SpentModel = SpentModel;
module.exports.TransactionModel = TransactionModel;
module.exports.AskingSoldModel = AskingSoldModel;
