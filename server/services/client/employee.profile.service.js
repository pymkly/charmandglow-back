const { AskingSoldModel, TransactionModel } = require("../../db/dbaccessor");

getLastSold = async (id) => {
  let lastTransaction = await TransactionModel.find({
    "user._id": id,
  }).sort({ date: -1 });
  if (lastTransaction.length > 0) {
    return lastTransaction[0].reste;
  }
  return 0;
};

chargeAccount = async (req, res) => {
  let data = req.body;
  data.date = new Date();
  let demand = new AskingSoldModel(data);
  demand
    .save()
    .then(() => {
      res.send({
        code: 200,
        data: {
          message: "Success",
        },
      });
    })
    .catch((err) => {
      res.send({
        code: 500,
        text: `Error durant l'inscription`,
      });
    });
};

changeSchedule = async (req, res, data) => {
  changeUser(req, res, (staff) => {
    staff.hourly = data.hourly;
    return true;
  });
};

changePassword = async (req, res, data) => {
  changeUser(req, res, (staff) => {
    if (staff.password === data.password) {
      staff.password = data.newpass;
      return true;
    } else {
      res.send({
        code: 500,
      });
      return false;
    }
  });
};

modifyProfile = async (req, res, data) => {
  changeUser(req, res, (staff) => {
    staff.name = data.name;
    staff.firstname = data.firstname;
    staff.email = data.email;
    return true;
  });
};

changeUser = async (req, res, callback) => {
  let staff = await UserModel.findById(req.params.id);
  let response = callback(staff);
  if (response) {
    let result = await UserModel.findOneAndUpdate(
      {
        _id: req.params.id,
      },
      staff
    );
    if (result) {
      res.send({
        code: 200,
        data: {
          staff: staff,
        },
      });
    } else {
      res.send({
        code: 500,
      });
    }
  }
};

getEmployee = async (req, res) => {
  let staff = await UserModel.findById(req.params.id);
  staff.password = "";
  let sold = await getLastSold(req.params.id);
  if (staff) {
    res.send({
      code: 200,
      data: {
        staff: staff,
        sold: sold,
      },
    });
  } else {
    res.send({
      code: 500,
    });
  }
};

exports.getEmployee = getEmployee;
exports.modifyProfile = modifyProfile;
exports.changePassword = changePassword;
exports.changeSchedule = changeSchedule;
exports.chargeAccount = chargeAccount;
exports.getLastSold = getLastSold;
