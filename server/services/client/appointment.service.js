const { AppointmentModel } = require("../../db/dbaccessor");
const dotenv = require("dotenv");
dotenv.config();
const { sendMail } = require("../other/mailer.service");
const { getLastSold } = require("./employee.profile.service");
const { getAdminUser } = require("../admin/account.sold.service");

getAdminTransaction = async (appointment, adminPrice) => {
  let adminUser = await getAdminUser();
  let userLastSold = await getLastSold(adminUser._id);
  userLastSold += adminPrice;
  return {
    date: new Date(),
    value: adminPrice,
    state: 1,
    reste: userLastSold,
    user: adminUser,
  };
};

getEmployeeTransaction = async (appointment, employeePrice) => {
  let userLastSold = await getLastSold(appointment.employee._id);
  userLastSold += employeePrice;
  return {
    date: new Date(),
    value: employeePrice,
    state: 1,
    reste: userLastSold,
    user: appointment.employee,
  };
};
getUserTransaction = async (appointment, price, res) => {
  let userLastSold = await getLastSold(appointment.user._id);
  userLastSold -= price;
  if (userLastSold < 0) {
    res.send({
      code: 500,
      msg: "Pas assez de solde.",
    });
    return [null, false];
  }
  let userTransaction = {
    date: new Date(),
    value: price,
    state: -1,
    reste: userLastSold,
    user: appointment.user,
  };
  return [userTransaction, true];
};

payFor = async (req, res) => {
  let appointment = await AppointmentModel.findById(req.params.id);
  let appointmentP = parseFloat(appointment.price, 10);
  let sCommission = parseFloat(appointment.service.commission, 10);
  let employeeSold = (appointmentP * sCommission) / 100;
  let adminSold = appointmentP - employeeSold;
  let [userTransaction, userCheck] = await getUserTransaction(
    appointment,
    appointmentP,
    res
  );
  if (userCheck) {
    let employeeTransaction = await getEmployeeTransaction(
      appointment,
      employeeSold
    );
    let adminTransaction = await getAdminTransaction(appointment, adminSold);
    appointment.state = process.env.APPOINTEMENT_PAYED;
    let result = await AppointmentModel.findOneAndUpdate(
      {
        _id: req.params.id,
      },
      appointment
    );
    if (result) {
      let userTransac = new TransactionModel(userTransaction);
      userTransac
        .save()
        .then(async () => {
          let employeeTransac = new TransactionModel(employeeTransaction);
          employeeTransac
            .save()
            .then(async () => {
              let adminTransac = new TransactionModel(adminTransaction);
              adminTransac
                .save()
                .then(async () => {
                  res.send({
                    code: 200,
                  });
                })
                .catch((err) => {
                  console.log(err);
                  res.send({
                    code: 500,
                    text: `Erreur durant le paiement`,
                  });
                });
            })
            .catch((err) => {
              console.log(err);
              res.send({
                code: 500,
                text: `Erreur durant le paiement`,
              });
            });
        })
        .catch((err) => {
          console.log(err);
          res.send({
            code: 500,
            text: `Erreur durant le paiement`,
          });
        });
    } else {
      res.send({
        code: 500,
      });
    }
  }
};

getTasks = async (req, res) => {
  let targetDate = new Date(req.params.date);
  const startOfDay = new Date(targetDate.setHours(0, 0, 0, 0));
  const startOfNextDay = new Date(targetDate.setDate(targetDate.getDate() + 1));
  let appointments = await tasksForUserInDate(
    startOfDay,
    startOfNextDay,
    req.params.id
  );
  appointments = sum(appointments);
  res.send({
    code: 200,
    data: {
      tasks: appointments,
    },
  });
};

tasksForUserInDate = async (startOfDay, startOfNextDay, staffId) => {
  let params = {
    "employee._id": staffId,
    $or: [
      {
        state: process.env.APPOINTEMENT_DONE,
      },
      {
        state: { $gt: process.env.APPOINTEMENT_DONE },
      },
    ],
    date: {
      $gte: startOfDay,
      $lt: startOfNextDay,
    },
  };
  let appointments = await AppointmentModel.find(params);
  return appointments;
};

sum = (appointments) => {
  let result = [];
  let sum = 0;
  for (let i = 0; i < appointments.length; i++) {
    let value =
      (appointments[i].price * appointments[i].service.commission) / 100;
    sum += value;
    let temp = {
      appointment: appointments[i],
      commission: value,
    };
    result = [...result, temp];
  }
  let temp = {
    appointment: {},
    commission: sum,
    isSum: true,
  };
  result = [...result, temp];
  return result;
};

getClientAppointments = async (req, res) => {
  let params = {
    "user._id": req.params.id,
  };
  console.log(params);
  let appointments = await AppointmentModel.find(params);
  let sold = await getLastSold(req.params.id);
  res.send({
    code: 200,
    data: {
      appointments: appointments,
      sold: sold,
    },
  });
};

getAppointments = async (req, res) => {
  let params = {
    "employee._id": req.params.id,
  };
  console.log(params);
  let appointments = await AppointmentModel.find(params);
  res.send({
    code: 200,
    data: {
      appointments: appointments,
    },
  });
};

complete = async (req, res) => {
  changeState(req, res, process.env.APPOINTEMENT_DONE, (appointment) => {
    sendCompleted(appointment);
  });
};

sendCompleted = async (appointment) => {
  let subject = "Rendez-vous terminé";
  content = `<p>Merci pour avoir consulté notre service.</p>`;
  // console.log(appointment);
  sendMail(appointment.user.email, subject, content);
};

confirm = async (req, res) => {
  changeState(req, res, process.env.APPOINTEMENT_CONFIRMED, (appointment) => {
    sendConfirmation(appointment);
  });
};

sendConfirmation = async (appointment) => {
  let subject = "Rendez-vous confirmé";
  let content = `<h3>Votre prise de rendez-vous pour le service <strong>${appointment.service.title}</strong> a été confirmé.</h3><br>`;
  content += `<p>Votre rendez-vous a été fixé à la date ${formatDate(
    appointment.date
  )} et durera ${appointment.service.time}.</p>`;
  content += `<p>Le personnel en charge de votre service est <strong>${appointment.employee.firstname}</strong>.</p>`;
  content += `<p>Pour plus d'information, contactez-le personnel avec l'email <strong>${appointment.employee.email}</strong>.</p>`;
  // console.log(appointment);
  sendMail(appointment.user.email, subject, content);
};

changeAppointmentState = async (res, appointment, nextState) => {
  let state = parseInt(appointment.state, 10);
  let confirmState = parseInt(nextState, 10);
  let check = state < confirmState;
  if (check) {
    appointment.state = confirmState;
  } else {
    res.send({
      code: 500,
    });
  }
  return check;
};

changeState = async (req, res, nextState, callback) => {
  let appointment = await AppointmentModel.findById(req.params.id);
  let check = await changeAppointmentState(res, appointment, nextState);
  if (check) {
    let result = await AppointmentModel.findOneAndUpdate(
      {
        _id: req.params.id,
      },
      appointment
    );
    if (result) {
      callback(appointment);
      res.send({
        code: 200,
      });
    } else {
      res.send({
        code: 500,
      });
    }
  }
};

formatDate = (date) => {
  if (date) {
    const options = {
      year: "numeric",
      month: "long",
      day: "numeric",
      hour: "2-digit",
      minute: "2-digit",
      hour12: false, // Utiliser le format  24 heures
      timeZone: "UTC",
    };
    // Formater la date selon les options spécifiées
    const formatter = new Intl.DateTimeFormat("fr-FR", options);
    return formatter.format(date);
  }
  return date;
};

saveAppointment = async (req, res, data) => {
  data.date = new Date(data.date);
  data.state = 0;
  let appointment = new AppointmentModel(data);
  appointment
    .save()
    .then(() => {
      res.send({
        code: 200,
      });
    })
    .catch((err) => {
      res.send({
        code: 500,
      });
    });
};

exports.saveAppointment = saveAppointment;
exports.getAppointments = getAppointments;
exports.confirm = confirm;
exports.complete = complete;
exports.getTasks = getTasks;
exports.tasksForUserInDate = tasksForUserInDate;
exports.getClientAppointments = getClientAppointments;
exports.payFor = payFor;
