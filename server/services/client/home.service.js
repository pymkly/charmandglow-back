const { ServiceModel } = require("../../db/dbaccessor");

getHomeRequirements = async (req, res) => {
  let services = await ServiceModel.find({});
  res.send({
    code: 200,
    data: { services: services },
  });
};

exports.getHomeRequirements = getHomeRequirements;
