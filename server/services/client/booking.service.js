const { ServiceModel, UserModel } = require("../../db/dbaccessor");
const { tasksForUserInDate } = require("./appointment.service");
const dotenv = require("dotenv");
dotenv.config();

function convertDurationToMinutes(duration) {
  const [hours, minutes] = duration.split(":").map(Number);
  return hours * 60 + minutes;
}

staffDisposition = async (req, res) => {
  const { appointmentDate, appointmentDuration } = req.body;
  let staffs = await UserModel.find({
    accreditation: parseInt(process.env.EMPLOYEE_ACCREDITATION, 10),
  });
  let availableEmployees = filterAvailableEmployees(
    staffs,
    appointmentDate,
    appointmentDuration
  );
  availableEmployees = await filterAppointments(
    availableEmployees,
    appointmentDate,
    appointmentDuration
  );
  res.send({
    code: 200,
    data: {
      staffs: availableEmployees,
    },
  });
};

filterAppointments = async (staffs, appointmentDate, duration) => {
  const durationInMinutes = convertDurationToMinutes(duration);
  const appointmentDateObj = new Date(appointmentDate);
  const appointmentEnd = addMinutes(appointmentDateObj, durationInMinutes);
  return staffs.filter(
    async (staff) =>
      await isAccessible(staff, appointmentDateObj, appointmentEnd)
  );
};

isAccessible = async (staff, start, end) => {
  // as = start, ae = end, bs = interval.start, be = interval.end
  let startDate = new Date(start.setHours(0, 0, 0, 0));
  let endDate = new Date(end.setHours(0, 0, 0, 0) + 1);
  let tasks = await tasksForUserInDate(startDate, endDate, staff._id);
  let intervals = toIntervals(tasks);
  return !intervals.some(
    (interval) => !(interval.start <= end || start >= interval.end)
  );
};

toIntervals = (tasks) => {
  let result = [];
  for (let task of tasks) {
    let start = new Date(task.date);
    let minutes = convertDurationToMinutes(task.service.time);
    let end = addMinutes(start, minutes);
    result = [
      ...result,
      {
        start: start,
        end: end,
      },
    ];
  }
  return result;
};

function addMinutes(date, minutes) {
  return new Date(date.getTime() + minutes * 60000);
}

function filterAvailableEmployees(
  employees,
  appointmentDate,
  appointmentDuration
) {
  const durationInMinutes = convertDurationToMinutes(appointmentDuration);
  const appointmentDateObj = new Date(appointmentDate);
  const appointmentEnd = addMinutes(appointmentDateObj, durationInMinutes);

  const filteredEmployees = employees.filter((employee) => {
    const workingHours = employee.hourly.filter(({ start, end }) => {
      const [startHours, startMinutes] = start.split(":").map(Number);
      const [endHours, endMinutes] = end.split(":").map(Number);
      const startTime = new Date(appointmentDateObj);
      startTime.setHours(startHours, startMinutes, 0, 0);
      const endTime = new Date(appointmentDateObj);
      endTime.setHours(endHours, endMinutes, 0, 0);
      let check =
        startTime <= appointmentDateObj && appointmentDateObj <= endTime;
      check &= startTime <= appointmentEnd && appointmentEnd <= endTime;
      return check;
    });
    console.log(workingHours);

    return workingHours.length > 0;
    // prendres les rendez-vous
    // obtenir les intervals de de rendez-vous
  });

  return filteredEmployees;
}

bookForUserInformation = async (req, res) => {
  let service = await ServiceModel.findById(req.params.id);
  let user = await UserModel.findById(req.params.userid);
  res.send({
    code: 200,
    data: { service: service },
  });
};

exports.bookForUserInformation = bookForUserInformation;
exports.staffDisposition = staffDisposition;
