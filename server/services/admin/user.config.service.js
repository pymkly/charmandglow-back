const { BodyPartModel } = require("../../db/dbaccessor");
const signUpUtils = async () => {
  let bodyPart = await BodyPartModel.find({});
  return {
    bodyPart: bodyPart,
  };
};

const getBodyParts = async (res) => {
  let result = await BodyPartModel.find({});
  res.send({
    code: 200,
    data: {
      bodyparts: result,
    },
  });
};

const getBodyPart = async (req, res) => {
  let result = await BodyPartModel.findById(req.params.id);
  if (result) {
    res.send({
      code: 200,
      data: {
        bodypart: result,
      },
    });
  } else {
    res.send({
      code: 500,
    });
  }
};

const addTypeToBodyPart = async (req, res, data) => {
  let bodyPart = await BodyPartModel.findById(req.params.id);
  bodyPart.types = [...bodyPart.types, data];
  console.log(bodyPart);
  let result = await BodyPartModel.findOneAndUpdate(
    {
      _id: req.params.id,
    },
    bodyPart
  );
  if (result) {
    res.send({
      code: 200,
    });
  } else {
    res.send({
      code: 500,
    });
  }
};

const addBodyPart = async (res, data) => {
  let bodyPart = new BodyPartModel(data);
  bodyPart
    .save()
    .then(() => {
      res.send({
        code: 200,
      });
    })
    .catch((err) => {
      res.send({
        code: 500,
      });
    });
};

exports.signUpUtils = signUpUtils;
exports.addBodyPart = addBodyPart;
exports.getBodyParts = getBodyParts;
exports.getBodyPart = getBodyPart;
exports.addTypeToBodyPart = addTypeToBodyPart;
