const { SkillModel, UserModel } = require("../../db/dbaccessor");
const dotenv = require("dotenv");
dotenv.config();

getStaffById = async (req, res) => {
  console.log(req.params.id);
  let staff = await UserModel.findById(req.params.id);
  console.log(staff);
  if (staff) {
    res.send({
      code: 200,
      data: {
        staff: staff,
      },
    });
  } else {
    res.send({
      code: 500,
    });
  }
};

getStaffList = async (req, res) => {
  let staff = await UserModel.find({
    accreditation: parseInt(process.env.EMPLOYEE_ACCREDITATION, 10),
  });
  let params = {
    staffs: staff,
  };
  res.send({
    code: 200,
    data: params,
  });
};
getStaffFormUtils = async (res) => {
  let skills = await SkillModel.find({});
  res.send({
    code: 200,
    data: {
      skills: skills,
    },
  });
};

getSkills = async (res) => {
  let result = await SkillModel.find({});
  res.send({
    code: 200,
    data: result,
  });
};

addSkills = async (res, data) => {
  let skill = new SkillModel(data);
  skill
    .save()
    .then(() => {
      res.send({
        code: 200,
      });
    })
    .catch((err) => {
      res.send({
        code: 500,
      });
    });
};

exports.addSkills = addSkills;
exports.getSkills = getSkills;
exports.getStaffFormUtils = getStaffFormUtils;
exports.getStaffList = getStaffList;
exports.getStaffById = getStaffById;
