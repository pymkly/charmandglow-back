const { AppointmentModel, TransactionModel } = require("../../db/dbaccessor");
const { getAdminUser } = require("./account.sold.service");
const dotenv = require("dotenv");
dotenv.config();

getMonthlyTurnOver = async (req, res) => {
  let { start, end } = req.body;
  let startDate = new Date(start);
  let endDate = new Date(end);
  let stats = await getMonthlyTurnOverBetween(startDate, endDate);
  res.send({
    code: 200,
    data: {
      statistics: stats,
      filledStats: stats,
    },
  });
};

getDailyTurnOver = async (req, res) => {
  let { start, end } = req.body;
  let startDate = new Date(start);
  let endDate = new Date(end);
  let stats = await getDailyTurnOverBetween(startDate, endDate);
  res.send({
    code: 200,
    data: {
      statistics: stats,
      filledStats: stats,
    },
  });
};

getMonthlyTurnOverBetween = async (startDate, endDate) => {
  let admin = await getAdminUser();
  return await TransactionModel.aggregate([
    {
      $match: {
        "user._id": admin._id,
        state: 1,
        date: { $gte: startDate, $lte: endDate },
      },
    },
    {
      $group: {
        _id: {
          year: { $year: "$date" },
          month: { $month: "$date" },
        },
        total: { $sum: "$value" },
      },
    },
    { $sort: { "_id.year": 1, "_id.month": 1 } },
  ]);
};

getDailyTurnOverBetween = async (startDate, endDate) => {
  let admin = await getAdminUser();
  return await TransactionModel.aggregate([
    {
      $match: {
        "user._id": admin._id,
        state: 1,
        date: { $gte: startDate, $lte: endDate },
      },
    },
    {
      $group: {
        _id: { $dateToString: { format: "%Y-%m-%d", date: "$date" } },
        total: { $sum: "$value" },
      },
    },
    { $sort: { _id: 1 } },
  ]);
};

getMonthlyStatistics = async (req, res) => {
  let { start, end } = req.body;
  let startDate = new Date(start);
  let endDate = new Date(end);
  let stats = await getDailyStatisticsBetween(startDate, endDate);
  stats = sumForMonth(stats);
  res.send({
    code: 200,
    data: {
      statistics: stats,
      filledStats: stats,
    },
  });
};

sumForMonth = (stats) => {
  let groupedReservations = {};
  let result = [];

  stats.forEach((reservation) => {
    let month = reservation._id.month + 1; // Les mois sont indexés à partir de 0
    let year = reservation._id.year;

    let key = `${year}-${month}`;

    if (!groupedReservations[key]) {
      let temp = reservation;
      groupedReservations[key] = temp;
      result = [...result, temp];
    } else {
      groupedReservations[key].count += reservation.count;
    }
  });
  return result;
};

getDailyStatistics = async (req, res) => {
  let { start, end } = req.body;
  let startDate = new Date(start);
  let endDate = new Date(end);
  let stats = await getDailyStatisticsBetween(startDate, endDate);
  let filledStats = completeMissingDays(stats, startDate, endDate);
  res.send({
    code: 200,
    data: {
      statistics: stats,
      filledStats: filledStats,
    },
  });
};
completeMissingDays = (stats, startDate, endDate) => {
  let dateRange = generateDateRange(startDate, endDate);
  let filledStats = dateRange.map((date) => {
    let dayStats = stats.find(
      (stat) =>
        stat._id.year === date.getFullYear() &&
        stat._id.month === date.getMonth() + 1 &&
        stat._id.day === date.getDate()
    );
    dayStats = dayStats || {
      _id: {
        year: date.getFullYear(),
        month: date.getMonth() + 1,
        day: date.getDate(),
      },
      count: 0,
    };
    dayStats.date = date;
    return dayStats;
  });
  return filledStats;
};
generateDateRange = (startDate, endDate) => {
  let dates = [];
  let currentDate = startDate;
  while (currentDate <= endDate) {
    dates = [...dates, new Date(currentDate)];
    currentDate.setDate(currentDate.getDate() + 1);
  }
  return dates;
};

getDailyStatisticsBetween = async (startDate, endDate) => {
  try {
    let stats = await AppointmentModel.aggregate([
      {
        $match: {
          date: {
            $gte: startDate,
            $lt: endDate,
          },
        },
      },
      {
        $group: {
          _id: {
            year: { $year: "$date" },
            month: { $month: "$date" },
            day: { $dayOfMonth: "$date" },
          },
          count: { $sum: 1 },
        },
      },
      {
        $sort: {
          "_id.year": 1,
          "_id.month": 1,
          "_id.day": 1,
        },
      },
    ]);

    return stats;
  } catch (error) {
    console.error("Erreur lors de la récupération des statistiques :", error);
    return [];
  }
};

exports.getDailyStatistics = getDailyStatistics;
exports.getMonthlyStatistics = getMonthlyStatistics;
exports.getDailyTurnOver = getDailyTurnOver;
exports.getMonthlyTurnOver = getMonthlyTurnOver;
