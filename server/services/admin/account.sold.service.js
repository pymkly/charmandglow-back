const {
  AskingSoldModel,
  TransactionModel,
  UserModel,
} = require("../../db/dbaccessor");
const { getLastSold } = require("../client/employee.profile.service");

const dotenv = require("dotenv");
dotenv.config();

getAdminUser = async () => {
  let params = {
    accreditation: parseInt(process.env.ADMIN_ACCREDITATION),
  };
  console.log(params);
  let users = await UserModel.find(params);
  console.log(users);
  return users[0];
};

accountAsking = async (req, res) => {
  let datas = await AskingSoldModel.find({});
  res.send({
    code: 200,
    data: {
      data: datas,
    },
  });
};

validateAsking = async (req, res) => {
  let demand = await AskingSoldModel.findById(req.params.id);
  let sold = await getLastSold(demand.user._id);
  let reste = sold + demand.value;
  let data = {
    date: new Date(),
    value: demand.value,
    state: 1,
    reste: reste,
    user: demand.user,
  };
  try {
    transaction = new TransactionModel(data);
    console.log("transaction ", data);
    transaction
      .save()
      .then(async () => {
        const result = await AskingSoldModel.deleteOne({ _id: demand._id });
        console.log(result);
        res.send({
          code: 200,
          data: {},
        });
      })
      .catch((err) => {
        res.send({
          code: 500,
          text: `Error durant l'inscription`,
        });
      });
  } catch (err) {
    console.error(err);
  }
};
exports.accountAsking = accountAsking;
exports.validateAsking = validateAsking;
exports.getAdminUser = getAdminUser;
