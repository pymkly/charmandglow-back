const {
  SkillModel,
  BodyPartModel,
  ServiceModel,
} = require("../../db/dbaccessor");

getServiceById = async (req, res) => {
  let service = await ServiceModel.findById(req.params.id);
  if (service) {
    res.send({
      code: 200,
      data: {
        service: service,
      },
    });
  } else {
    res.send({
      code: 500,
    });
  }
};

getServices = async (res, params) => {
  let services = await ServiceModel.find({});
  res.send({
    code: 200,
    data: { services: services },
  });
};

serviceFormUtils = async (res) => {
  let skills = await SkillModel.find({});
  let bodyParts = await BodyPartModel.find({});
  let params = {
    skills: skills,
    bodyParts: bodyParts,
  };
  res.send({
    code: 200,
    data: params,
  });
};

min = (data) => {
  if (data.prices.length > 0) {
    let indexI = 0;
    let indexJ = 0;
    for (let i = 0; i < data.prices.length; i++) {
      for (let j = 0; j < data.prices[i].pricesPerType.length; j++) {
        let current = parseFloat(
          data.prices[indexI].pricesPerType[indexJ].price,
          10
        );
        let temp = parseFloat(data.prices[i].pricesPerType[j].price, 10);
        if (temp <= current) {
          indexI = i;
          indexJ = j;
        }
      }
    }
    return parseFloat(data.prices[indexI].pricesPerType[indexJ].price, 10);
  }
  return 0;
};

max = (data) => {
  if (data.prices.length > 0) {
    let indexI = 0;
    let indexJ = 0;
    for (let i = 0; i < data.prices.length; i++) {
      for (let j = 0; j < data.prices[i].pricesPerType.length; j++) {
        let current = parseFloat(
          data.prices[indexI].pricesPerType[indexJ].price,
          10
        );
        let temp = parseFloat(data.prices[i].pricesPerType[j].price, 10);
        if (temp >= current) {
          indexI = i;
          indexJ = j;
        }
      }
    }
    return parseFloat(data.prices[indexI].pricesPerType[indexJ].price, 10);
  }
  return 0;
};

toMillis = (time) => {
  let times = time.split(":");
  return (parseInt(times[0], 10) * 3600 + parseInt(times[1], 10) * 60) * 1000;
};

addService = async (req, res, data) => {
  data["minprice"] = min(data);
  data["maxprice"] = max(data);
  let millis = toMillis(data.time);
  data.duration = new Date();
  data.duration.setTime(millis);
  let service = new ServiceModel(data);
  service
    .save()
    .then(() => {
      res.send({
        code: 200,
      });
    })
    .catch((err) => {
      res.send({
        code: 500,
      });
    });
};

exports.serviceFormUtils = serviceFormUtils;
exports.addService = addService;
exports.getServices = getServices;
exports.getServiceById = getServiceById;
