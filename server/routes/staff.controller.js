const express = require("express");
const router = express.Router();
const {
  getStaffFormUtils,
  getStaffList,
  getStaffById,
} = require("../services/admin/staff.config.service");

router.get("/utils/form", async (req, res) => {
  getStaffFormUtils(res);
});

router.get("/id/:id", async (req, res) => {
  getStaffById(req, res);
});

router.post("/search", async (req, res) => {
  getStaffList(req, res);
});

exports.userRouter = router;
