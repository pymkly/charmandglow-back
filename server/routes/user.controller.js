const express = require("express");
const router = express.Router();
const { UserModel } = require("../db/dbaccessor");
const {
  login,
  checkAccreditation,
  registerClient,
  bodyParts,
  registerAdmin,
  registerEmployee,
} = require("../authentification/auth.service");
const {
  addSkills,
  getSkills,
} = require("../services/admin/staff.config.service");
const { getBodyPart, addTypeToBodyPart } = require("../services/admin/user.config.service");
const dotenv = require("dotenv");
const { signUpUtils } = require("../services/admin/user.config.service");
dotenv.config();

const authClient = async (req, res, next) => {
  let accreditation = process.env.CLIENT_ACCREDITATION;
  checkAuth(req, res, accreditation, next);
};

const authEmployee = async (req, res, next) => {
  let accreditation = process.env.EMPLOYEE_ACCREDITATION;
  checkAuth(req, res, accreditation, next);
};

const authAdmin = async (req, res, next) => {
  let accreditation = process.env.ADMIN_ACCREDITATION;
  console.log("accreditation", accreditation);
  checkAuth(req, res, accreditation, next);
};

router.post("/login", async (req, res) => {
  let user = req.body;
  login(user, res);
});

router.post("/register/client", async (req, res) => {
  registerClient(req, res);
});

router.post("/register/employee", async (req, res) => {
  registerEmployee(req, res);
});

router.post("/skill", async (req, res) => {
  let skill = req.body;
  addSkills(res, skill);
});

router.get("/skills", async (req, res) => {
  getSkills(res);
});

router.post("/register/admin", authAdmin, async (req, res) => {
  registerAdmin(req, res);
});

router.get("/bodyparts", async (req, res) => {
  bodyParts(req, res);
});

router.get("/body-part/:id", async (req, res) => {
  getBodyPart(req, res);
});

router.post("/body-part/:id/type", async (req, res) => {
  let type = req.body;
  addTypeToBodyPart(req, res, type);
});

router.get("/register/utils", async (req, res) => {
  let utils = signUpUtils();
  res.send({
    code: 200,
    data: utils,
  });
});

router.get("/", function (req, res) {
  UserModel.find().then(function (err, users) {
    if (err) {
      res.send(err);
    }
    res.json(users);
  });
});

const checkAuth = async (req, res, accreditation, next) => {
  if (checkAccreditation(req, accreditation)) {
    next();
  } else {
    res.send({
      code: 501,
      text: `Vous n'avez pas d'accès pour le faire`,
    });
  }
};

exports.userRouter = router;
exports.authClient = authClient;
exports.authEmployee = authEmployee;
exports.authAdmin = authAdmin;
