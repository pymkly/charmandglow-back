const express = require("express");
const router = express.Router();
const {
  saveAppointment,
  getAppointments,
  confirm,
  complete,
  getTasks,
  getClientAppointments,
  payFor,
} = require("../services/client/appointment.service");

router.post("", async (req, res) => {
  let params = req.body;
  saveAppointment(req, res, params);
});

router.get("/pay/:id", async (req, res) => {
  payFor(req, res);
});
router.get("/user/id/:id", async (req, res) => {
  getClientAppointments(req, res);
});
router.get("/employee/id/:id", async (req, res) => {
  getAppointments(req, res);
});
router.get("/employee/id/:id/tasks/:date", async (req, res) => {
  getTasks(req, res);
});

router.put("/complete/:id", async (req, res) => {
  complete(req, res);
});

router.put("/confirm/:id", async (req, res) => {
  confirm(req, res);
});
exports.userRouter = router;
