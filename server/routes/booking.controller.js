const express = require("express");
const router = express.Router();
const {
  bookForUserInformation,
  staffDisposition,
} = require("../services/client/booking.service");
router.post("/information/:id/user/:userid", async (req, res) => {
  bookForUserInformation(req, res);
});

router.post("/staffs/availability", async (req, res) => {
  staffDisposition(req, res);
});

exports.userRouter = router;
