const express = require("express");
const router = express.Router();
const { getHomeRequirements } = require("../services/client/home.service");
router.get("/requirements", async (req, res) => {
  getHomeRequirements(req, res);
});

exports.userRouter = router;
