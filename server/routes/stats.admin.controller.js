const express = require("express");
const router = express.Router();

const {
  getDailyStatistics,
  getMonthlyStatistics,
  getDailyTurnOver,
  getMonthlyTurnOver,
} = require("../services/admin/stat.appointment.service");

router.post("/turnover/daily", async (req, res) => {
  getDailyTurnOver(req, res);
});

router.post("/turnover/monthly", async (req, res) => {
  getMonthlyTurnOver(req, res);
});

router.post("/appointments/daily", async (req, res) => {
  getDailyStatistics(req, res);
});

router.post("/appointments/monthly", async (req, res) => {
  getMonthlyStatistics(req, res);
});

exports.userRouter = router;
