const express = require("express");
const router = express.Router();
const {
  getEmployee,
  modifyProfile,
  changePassword,
  changeSchedule,
} = require("../services/client/employee.profile.service");

router.get("/id/:id", async (req, res) => {
  getEmployee(req, res);
});

router.put("/id/:id/profile", async (req, res) => {
  let user = req.body;
  modifyProfile(req, res, user);
});

router.put("/id/:id/password", async (req, res) => {
  let user = req.body;
  changePassword(req, res, user);
});

router.put("/id/:id/schedule", async (req, res) => {
  let user = req.body;
  changeSchedule(req, res, user);
});

exports.userRouter = router;
