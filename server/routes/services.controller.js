const express = require("express");
const router = express.Router();
const { authClient, authAdmin } = require("./user.controller");
const {
  addBodyPart,
  getBodyParts,
} = require("../services/admin/user.config.service");
const {
  serviceFormUtils,
  addService,
  getServices,
  getServiceById,
} = require("../services/admin/services.service");

router.post("/search", async (req, res) => {
  let params = req.body;
  getServices(res, params);
});

router.get("/id/:id", async (req, res) => {
  getServiceById(req, res);
});

router.post("/", authAdmin, async (req, res) => {
  let service = req.body;
  addService(req, res, service);
});

router.post("/body-part", authAdmin, async (req, res) => {
  let bodyPart = req.body;
  console.log("bodyPart", bodyPart);
  addBodyPart(res, bodyPart);
});

router.get("/body-parts", async (req, res) => {

  getBodyParts(res);
});

router.get("/form-utils", async (req, res) => {
  serviceFormUtils(res);
});

exports.userRouter = router;
