const express = require("express");
const router = express.Router();
const {
  chargeAccount,
} = require("../services/client/employee.profile.service");

router.post("/charge", async (req, res) => {
  chargeAccount(req, res);
});

exports.userRouter = router;
