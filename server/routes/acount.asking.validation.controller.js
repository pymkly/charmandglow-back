const express = require("express");
const router = express.Router();
const {
  accountAsking,
  validateAsking,
} = require("../services/admin/account.sold.service");
router.get("/asking-list", async (req, res) => {
  accountAsking(req, res);
});

router.get("/validate/:id", async (req, res) => {
  validateAsking(req, res);
});

exports.userRouter = router;
