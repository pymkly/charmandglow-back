const express = require("express");
const path = require("path");
const connectDB = require("./server/db/connection");
const app = express();
const cors = require("cors");
const corsOptions = {
  origin: [
    "https://charmandglow-depl-7lpxfemkm-pymklys-projects.vercel.app",
    "http://localhost:4200",
    "https://charmandglow-depl.vercel.app",
  ],
};
app.use(cors(corsOptions));
app.use(express.json());
const PORT = 3000;

try {
  connectDB();
  app.use(express.static(path.join(__dirname, "dist")));
  app.use("/", function (req, res, next) {
    console.log("Time:", Date.now());
    next();
  });
  app.use("/api/example", require("./server/routes/example-route"));
  app.use("/user", require("./server/routes/user.controller").userRouter);
  app.use(
    "/services",
    require("./server/routes/services.controller").userRouter
  );
  app.use(
    "/statistics",
    require("./server/routes/stats.admin.controller").userRouter
  );
  app.use("/home", require("./server/routes/home.controller").userRouter);
  app.use("/booking", require("./server/routes/booking.controller").userRouter);
  app.use(
    "/employee",
    require("./server/routes/employee.profile.controller").userRouter
  );
  app.use(
    "/client/account",
    require("./server/routes/client.profile.controller").userRouter
  );
  app.use(
    "/account-validation",
    require("./server/routes/acount.asking.validation.controller").userRouter
  );
  app.use(
    "/appointment",
    require("./server/routes/appointment.controller").userRouter
  );
  app.use("/staff", require("./server/routes/staff.controller").userRouter);
  app.get("/api/*", (req, res) => {
    res.send({
      message: "Endpoint not found",
      type: "error",
    });
  });
  app.get("*", (req, res) => {
    console.log(req.url);
    res.sendFile(path.join(__dirname, "dist/index.html"));
  });
  app.listen(PORT, () =>
    console.log(`Application started successfully on port: ${PORT}!`)
  );
} catch (error) {
  console.log(error);
}
